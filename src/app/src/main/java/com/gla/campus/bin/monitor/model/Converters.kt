package com.gla.campus.bin.monitor.model

import androidx.room.TypeConverter
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class Converters {
    @TypeConverter
    fun fromString(value: String): MutableMap<String,Double> {
        val mutableMapType: Type = object : TypeToken<MutableMap<String, Double>>() {}.type
        return Gson().fromJson(value, mutableMapType)
    }

    @TypeConverter
    fun fromMutableMap(mutableMap: MutableMap<String,Double>): String {
        val gson = Gson()
        return gson.toJson(mutableMap)
    }
}
