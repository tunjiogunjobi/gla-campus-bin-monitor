package com.gla.campus.bin.monitor.model

import java.io.Serializable


/**
 * Represent smart bin fill levels
 * @property fillLevelMap, where key is a string that represents time and value represent fill level
 */
data class FillLevel(var fillLevelMap: MutableMap<String, Double>):Serializable {


}