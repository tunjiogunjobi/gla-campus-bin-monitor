package com.gla.campus.bin.monitor.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity()
data class SmartBin constructor(@PrimaryKey var id: String, val latitude: Double,
                                val longitude: Double, val locationName: String,
                                var binFillLevels: MutableMap<String,Double>)
{
/*    constructor(id: String, latitude: Double, longitude: Double, locationName: String,
    binLevel: Double ): this(id, binLevel)*/
}