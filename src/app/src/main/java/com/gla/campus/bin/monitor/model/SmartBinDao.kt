package com.gla.campus.bin.monitor.model

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface SmartBinDao {
    @Insert(onConflict = REPLACE)
    fun save(smartBin: SmartBin)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIgnore(entity: SmartBin) : Long

    @Transaction
    fun insertUpdate(smartbin:SmartBin){
        if (insertIgnore(smartbin) == -1L) {
            updateSmartBin(smartbin)
        }
    }

    @Update
    fun updateSmartBin(smart: SmartBin)

    @Query("SELECT * FROM smartbin WHERE id = :id")
    fun load(id: String): SmartBin


    @Query(value = "SELECT * FROM smartbin")
    fun loadAll(): LiveData<List<SmartBin>>


}