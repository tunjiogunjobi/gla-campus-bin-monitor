package com.gla.campus.bin.monitor.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.gla.campus.bin.monitor.utils.Utils
import java.util.concurrent.Executors

@Database(entities = [SmartBin::class], version = 1)
@TypeConverters(Converters::class)
abstract class SmartBinDatabase : RoomDatabase()     {
    abstract fun smartBinDao(): SmartBinDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: SmartBinDatabase? = null

        fun getDatabase(context: Context): SmartBinDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SmartBinDatabase::class.java,
                    "gla_campus_bin_monitor_database"
                ).addCallback(object: Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        Executors.newSingleThreadScheduledExecutor()
                            .execute(Runnable {
                                val dao = getDatabase(context).smartBinDao()
                                for (smartBin in Utils.buildSmartBins()){
                                    dao.save(smartBin)

                                }
                            })
                    }
                }).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}


