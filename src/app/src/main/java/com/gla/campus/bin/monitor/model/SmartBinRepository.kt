package com.gla.campus.bin.monitor.model

import android.util.Log
import androidx.lifecycle.LiveData
import com.gla.campus.bin.monitor.mqtt.SmartBinMqttService
import com.gla.campus.bin.monitor.utils.Utils
import org.eclipse.paho.client.mqttv3.IMqttMessageListener
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.json.JSONObject
import org.json.JSONTokener
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Singleton
class SmartBinRepository(private val smartBinDao: SmartBinDao) {

    private val executor: Executor = Executors.newCachedThreadPool()

    init {
        //mqttHandler.setOnMessageReceivedListener(this)

    }

    fun getAllSmartBins():LiveData<List<SmartBin>> {
        //refreshSmartBin(id)
        // Returns a LiveData object directly from the database.
        return smartBinDao.loadAll()
    }

    private fun getSmartBin(id: String): SmartBin{
        return smartBinDao.load(id)
    }

    fun saveSmartBin(smartBin: SmartBin) {
        Log.d(Utils.TAG, "SaveSmartBin called")
        executor.execute { smartBinDao.save(smartBin)}
    }

     fun updateSmartBin(smartBin: SmartBin) {
        executor.execute { smartBinDao.updateSmartBin(smartBin)}
    }

    fun updateSmartBin(id: String, time: String, distance: Double) {
        executor.execute {
            val bin = getSmartBin(id)
            val binFillLevels = bin.binFillLevels
            binFillLevels[time] = distance
            updateSmartBin(SmartBin(id,bin.latitude,bin.longitude,bin.locationName,binFillLevels))
           Log.d(Utils.TAG, "updateSmartBin called ${binFillLevels.count().toString()}" )

        }
    }

}