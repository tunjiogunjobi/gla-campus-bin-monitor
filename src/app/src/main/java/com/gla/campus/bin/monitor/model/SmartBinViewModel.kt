package com.gla.campus.bin.monitor.model

import android.app.Application
import android.content.*
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gla.campus.bin.monitor.mqtt.SmartBinMqttService
import com.gla.campus.bin.monitor.utils.Utils
import org.eclipse.paho.client.mqttv3.IMqttMessageListener
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.json.JSONObject
import org.json.JSONTokener

class SmartBinViewModel(application: Application) : AndroidViewModel(application){

    private val smartBinRepository: SmartBinRepository
    private var smartBinMqttService: SmartBinMqttService? = null
    private var mqttBroadCast: MqttBroadcast
    private val context = application.applicationContext

    val serviceConnection = object: ServiceConnection{
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            smartBinMqttService = (service as SmartBinMqttService.LocalBinder).service

        }

        override fun onServiceDisconnected(name: ComponentName?) {
            smartBinMqttService = null
        }
    }

    init {
        val smartBinDao = SmartBinDatabase.getDatabase(application).smartBinDao()
        //val mqttHandler = MqttHandler(application.applicationContext)
        //smartBinRepository = SmartBinRepository(smartBinDao,mqttHandler)
        smartBinRepository = SmartBinRepository(smartBinDao)

        mqttBroadCast = MqttBroadcast()

        LocalBroadcastManager.getInstance(context).registerReceiver(mqttBroadCast,
            IntentFilter(SmartBinMqttService.CONNECTION_SUCCESS)
        )
        val startServiceIntent = Intent(context, SmartBinMqttService::class.java)
        //context.bindService(startServiceIntent,serviceConnection,0)

        startServiceIntent.action = SmartBinMqttService.MQTT_CONNECT

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            context.startForegroundService(startServiceIntent)
        }else{
            context.startService(startServiceIntent)
        }
    }



    fun stopSmartBinMqttService(){
        //context.unbindService(serviceConnection)
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mqttBroadCast)
/*        val startServiceIntent = Intent(context,SmartBinMqttService::class.java)
        startServiceIntent.action = SmartBinMqttService.MQTT_DISCONNECT
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            context.startForegroundService(startServiceIntent)
        }else{
            context.startService(startServiceIntent)
        }*/
    }




    fun getSmartBins(): LiveData<List<SmartBin>> {

        return smartBinRepository.getAllSmartBins()
    }


    inner class MqttBroadcast: BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent!!.action){
                SmartBinMqttService.CONNECTION_SUCCESS-> {//display some message using activity or fragment
                    }//smartBinRepository.updateSmartBin()
                SmartBinMqttService.CONNECTION_FAILURE-> { }
                SmartBinMqttService.CONNECTION_LOST ->{}
                SmartBinMqttService.DISCONNECT_SUCCESS->{}
                SmartBinMqttService.MQTT_MESSAGE_PAYLOAD->{

                }
            }
        }
    }



}