package com.gla.campus.bin.monitor.mqtt

import org.eclipse.paho.client.mqttv3.IMqttActionListener
import org.eclipse.paho.client.mqttv3.IMqttMessageListener

interface BaseMqttHandler {
    fun connectToServer()

    fun disConnectFromServer()

    fun subscribeToTopic()

    fun subscribeToTopic(topic: String)

    fun unsubscribeFromtopic(topic: String)
}