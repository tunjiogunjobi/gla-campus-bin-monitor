package com.gla.campus.bin.monitor.mqtt

import android.content.Context
import android.util.Log
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*

class CustomMqttAndroidClient(context: Context, serverURI: String, clientID: String):
    MqttAndroidClient(context,serverURI,clientID) {

    private lateinit var mqttCallback: MqttCallback

    override fun connect(
        options: MqttConnectOptions?,
        userContext: Any?,
        callback: IMqttActionListener?
    ): IMqttToken {
        log("Connecting to Mqtt Broker.....")
        return super.connect(options, null, callback)
    }

    override fun reconnect() {
        TODO("Not yet implemented")
    }



    fun log(msg: String){
        Log.d("MQTT", msg)
    }

    override fun subscribe(
        topic: String?,
        qos: Int,
        userContext: Any?,
        callback: IMqttActionListener?
    ): IMqttToken {
        log("Subscribing to topic $topic")
        return super.subscribe(topic, qos, userContext, callback)
    }

    override fun setCallback(callback: MqttCallback?) {
        super.setCallback(callback)
        this.mqttCallback = callback!!

    }

    override fun removeMessage(token: IMqttDeliveryToken?): Boolean {
        TODO("Not yet implemented")
    }


}