package com.gla.campus.bin.monitor.mqtt

import android.content.Context
import android.util.Log
import com.gla.campus.bin.monitor.utils.Utils
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.json.JSONObject
import org.json.JSONTokener

class CustomMqttCallbackExtended(): MqttCallbackExtended {

    override fun connectComplete(reconnect: Boolean, serverURI: String?) {
        if (reconnect) {
            Log.d(Utils.TAG, "Reconnected to : $serverURI")
            // Because Clean Session is true, we need to re-subscribe
            // subscribeToTopic(Utils.mqttSubscriptionTopic) // change this later to connect to same topic before loss of connection
        } else {
            Log.d(Utils.TAG, "Connected to: $serverURI")
        }
    }

    override fun connectionLost(cause: Throwable?) {
        Log.d(Utils.TAG, "The Connection was lost.")
    }

    override fun deliveryComplete(token: IMqttDeliveryToken?) {
        TODO("Not yet implemented")
    }

    @Throws(Exception::class)
    override fun messageArrived(topic: String, message: MqttMessage) {

        log("Incoming message inside Custom Call back: " + String(message.payload))

        val msg = String(message.payload)
        val jsonObject: JSONObject = JSONTokener(msg).nextValue() as JSONObject
        val payloadFields: JSONObject =
            JSONTokener(jsonObject.get("payload_fields").toString()).nextValue() as JSONObject
        val distance = payloadFields.get("distance").toString()
        val myCounter = payloadFields.get("my_counter").toString()
        val id = jsonObject.get("dev_id").toString()

        val metaData = JSONTokener(jsonObject.get("metadata").toString()).nextValue() as JSONObject
        val time = metaData.get("time").toString()

        //val smartBin = SmartBin(id, 0.0, 0.0, "JWS",
        //  mutableMapOf(Pair(time,distance.toDouble())))
        // onMessageReceivedListener.updateSmartBin(id,time,distance.toDouble())
    }

    private fun log(msg: String){
        Log.d("MQTT",msg)
    }

}