package com.gla.campus.bin.monitor.mqtt

import android.util.Log
import org.eclipse.paho.android.service.MqttTraceHandler
import java.lang.Exception

class CustomMqttTraceHandler: MqttTraceHandler {

    private val TAG = "MQTT"
    override fun traceDebug(tag: String?, message: String?) {
        Log.d(TAG,"$tag - $message")
    }

    override fun traceException(tag: String?, message: String?, e: Exception?) {
        Log.d(TAG,"$tag - $message \n${e.toString()}")
    }

    override fun traceError(tag: String?, message: String?) {
        Log.d(TAG,"$tag - $message")
    }
}