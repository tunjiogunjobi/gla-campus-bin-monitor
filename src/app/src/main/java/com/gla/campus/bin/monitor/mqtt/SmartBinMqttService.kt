package com.gla.campus.bin.monitor.mqtt

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.gla.campus.bin.monitor.R
import com.gla.campus.bin.monitor.model.SmartBinDatabase
import com.gla.campus.bin.monitor.model.SmartBinRepository
import com.gla.campus.bin.monitor.utils.Utils
import com.gla.campus.bin.monitor.views.MyApplication
import org.eclipse.paho.client.mqttv3.*
import org.json.JSONObject
import org.json.JSONTokener
import java.lang.Exception

class SmartBinMqttService : Service(), BaseMqttHandler {

    private lateinit var mqttClient: CustomMqttAndroidClient
    private var mqttClientID = MqttClient.generateClientId()
    private var messageListener: IMqttMessageListener? = null
    private val accessKey =
        "ttn-account-v2.ZVq5yCNo1wRLVNR_8QzZxCp3ASFwfTijRBhQ2qx3Usg".toCharArray()
    private lateinit var smartBinRepository: SmartBinRepository


    companion object {
        const val MQTT_CONNECT = "mqtt_connect"
        const val MQTT_DISCONNECT = "mqtt_disconnect"
        const val MQTT_SERVER_URI = "tcp://eu.thethings.network:1883"

        const val CONNECTION_SUCCESS = "CONNECTION_SUCCESS"
        val CONNECTION_FAILURE = "CONNECTION_FAILURE"
        val CONNECTION_LOST = "CONNECTION_LOST"
        val DISCONNECT_SUCCESS = "DISCONNECT_SUCCESS"

        val MQTT_MESSAGE_TYPE = "type"
        val MQTT_MESSAGE_PAYLOAD = "payload"
        val MQTT_SUBSCRIPTION_TOPIC = "+/devices/+/up"
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                val builder = Notification.Builder(this, MyApplication.CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))

                    .setSmallIcon(R.drawable.icon_garbage)
                    .setContentText("MQTT Running")
                    .setAutoCancel(true)
                startForeground(1, builder.build())

            } catch (e: Exception) {
                Log.d("MQTT", "Unable to build notification..")
            }
        } else {
            startForeground(0, Notification())
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) {
            logErrorMqtt("Null in onStartCommand, returning START_NOT_STICKY")
            stopSelf()
            return Service.START_NOT_STICKY
        }

        when (intent.action) {
            MQTT_CONNECT -> connectToServer()
            MQTT_DISCONNECT -> disConnectFromServer()
        }

        // will return start sticky and will recreate the service with saved intent when Android destroy it
        return super.onStartCommand(intent, flags, startId)


    }

    override fun connectToServer() {
        mqttClient = CustomMqttAndroidClient(this.applicationContext, MQTT_SERVER_URI, mqttClientID)
        mqttClient.setCallback(CustomMqttCallbackExtended())

        //enable logs from MQTT library
        mqttClient.setTraceEnabled(true)
        mqttClient.setTraceCallback(CustomMqttTraceHandler())

        val connectOptions = MqttConnectOptions()
        connectOptions.apply {
            connectionTimeout = 30
            isAutomaticReconnect = true
            isCleanSession = false
            keepAliveInterval = 120
            userName = Utils.appId
            password = accessKey
        }

        mqttClient.connect(connectOptions, null, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                logMqtt("Success connecting to broker...")
                LocalBroadcastManager.getInstance(this@SmartBinMqttService).sendBroadcast(
                    Intent(CONNECTION_SUCCESS)
                )
                smartBinRepository = SmartBinRepository(
                    SmartBinDatabase.getDatabase(this@SmartBinMqttService).smartBinDao()
                )
                subscribeToTopic()
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                logErrorMqtt("Error on connecting to broker...retry?: ${exception!!.toString()}")
                LocalBroadcastManager.getInstance(this@SmartBinMqttService).sendBroadcast(
                    Intent(
                        CONNECTION_FAILURE
                    )
                )
                disConnectFromServer()
            }

        })
    }

    override fun disConnectFromServer() {
        mqttClient.disconnect(null, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                logMqtt("Disconnected from server attempt success...")
                mqttClient.unregisterResources()
                SmartBinMqttService@ mqttClient.close()
                LocalBroadcastManager.getInstance(this@SmartBinMqttService).sendBroadcast(
                    Intent(
                        DISCONNECT_SUCCESS
                    )
                )
                stopSelf()
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                logMqtt("Failure on trying to disconnect: ${exception.toString()}")
            }

        })
    }


    override fun subscribeToTopic(topic: String) {
        logMqtt("Subscribing to topic $topic")
        mqttClient.subscribe(topic, 2)

    }


    override fun subscribeToTopic() {
        logMqtt("Subscribing to topic ${Utils.mqttSubscriptionTopic}")
        mqttClient.subscribe(Utils.mqttSubscriptionTopic, 2, null, object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken?) {
                TODO("Not yet implemented")
            }

            override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                TODO("Not yet implemented")
            }

        }, object: IMqttMessageListener{
            override fun messageArrived(topic: String?, message: MqttMessage?) {
                Log.d(Utils.TAG, "Incoming message: " + message?.payload?.let { String(it) })

                val msg = message?.payload?.let { String(it) }
                val jsonObject: JSONObject = JSONTokener(msg).nextValue() as JSONObject
                val payloadFields: JSONObject =
                    JSONTokener(jsonObject.get("payload_fields").toString()).nextValue() as JSONObject
                val distance = payloadFields.get("distance").toString()
                val myCounter = payloadFields.get("my_counter").toString()
                val id = jsonObject.get("dev_id").toString()

                val metaData = JSONTokener(jsonObject.get("metadata").toString()).nextValue() as JSONObject
                val time = metaData.get("time").toString()

                smartBinRepository.updateSmartBin(id,time,distance.toDouble())     }

        })
    }

    override fun unsubscribeFromtopic(topic: String) {
        mqttClient.unsubscribe(topic)
    }

    private fun logErrorMqtt(msg: String) {
        Log.w("MQTT", msg)
    }

    private fun logMqtt(msg: String) {
        Log.d("MQTT", msg)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true)
        }
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.d(this.javaClass.name, "UNBIND")
        return true
    }


    fun setMessageListener(listener: IMqttMessageListener) {
        messageListener = listener
    }


    override fun onBind(intent: Intent?): IBinder? = mBinder


    private val mBinder = LocalBinder()

    inner class LocalBinder : Binder() {
        val service: SmartBinMqttService
            get() = this@SmartBinMqttService
    }
}