package com.gla.campus.bin.monitor.utils

import android.os.Environment
import com.gla.campus.bin.monitor.model.SmartBin
import com.google.android.gms.maps.model.LatLng
import org.eclipse.paho.client.mqttv3.MqttClient
import java.io.File
import java.io.FileOutputStream

object Utils {
    const val TAG = "CampusBinMonitor"

    const val appId = "ultrasonic-tester-uog" // username
    val clientId:String = MqttClient.generateClientId()
    const val serverURI = "tcp://eu.thethings.network:1883"
    val accessKey = "ttn-account-v2.ZVq5yCNo1wRLVNR_8QzZxCp3ASFwfTijRBhQ2qx3Usg".toCharArray()
    const val mqttSubscriptionTopic = "+/devices/+/up"


    val binLocations = mapOf("Fraser Building 1" to Pair(55.873096, -4.288483),
        "UofG Library" to Pair(55.873272, -4.288444),
        "Fraser Building 2" to Pair(55.873271, -4.288340),
        "Fraser Building 3" to Pair(55.873207, -4.288366),
        "Fraser Building SouthPark" to Pair(55.872924, -4.287282),
        "Round Room" to Pair(55.872552, -4.288119),
        "Main Building Memorial Gate" to Pair(55.871979, -4.288283),
        "Rankine" to Pair(55.872569, -4.285941),
        "Stevenson Building Gibson Street" to Pair(55.873124, -4.285230),
        "St Andrews Building, Eldon St" to Pair(55.871985, -4.279671),
        "JWS" to Pair(55.870751, -4.287085),
        "Main Building South" to Pair(55.870790, -4.288775),
        "One A Square" to Pair(55.871554, -4.289601),
        "Joseph Black" to Pair(55.872045, -4.292809),
        "Kelvin Building" to Pair(55.871377, -4.291741),
        "Sir James Black Building" to Pair(55.871055, -4.292336),
        "University Pl" to Pair(55.872716, -4.294605),
        "QMU" to Pair(55.873583, -4.291472),
        "Wolfson Medical Blding" to Pair(55.872873, -4.292904),
        "Bower Blding" to Pair(55.872277, -4.290957),
        "Bute Gardens" to Pair(55.873732, -4.289462),
        "Path" to Pair(55.873365, -4.290468),
        "Stair Building" to Pair(55.871520, -4.290445),
        "Glasgow Int. College" to Pair(55.870155, -4.296413),
        "Western Infirmary LT" to Pair(55.871810, -4.294517),
        "Sch of Computing" to Pair(55.874490, -4.291551))

    fun buildSmartBins():MutableList<SmartBin>{
        val smartBins = mutableListOf<SmartBin>()

        for ((k,pair) in binLocations){
            val lat = pair.first
            val lng = pair.second
            var binLevel  = 0.0

            val id = k.toLowerCase().replace("\\s".toRegex(), "_")
            smartBins.add(SmartBin(id,lat,lng, k, mutableMapOf(Pair("",binLevel))))
        }

        return smartBins
    }

}