package com.gla.campus.bin.monitor.views

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavController.OnDestinationChangedListener
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import com.gla.campus.bin.monitor.R
import com.gla.campus.bin.monitor.model.SmartBin
import com.gla.campus.bin.monitor.model.SmartBinViewModel
import com.gla.campus.bin.monitor.utils.Utils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import java.util.*


class MainActivity : AppCompatActivity(), OnMapReadyCallback, OnMarkerClickListener,
    OnDestinationChangedListener{

    private lateinit var navController: NavController
    private lateinit var mMap: GoogleMap
    private lateinit var mViewModel: SmartBinViewModel
//    private val smartBins = Utils.buildSmartBins()
    private val smartBinMarkers = mutableMapOf<String, Marker>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        navController = (supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment).navController
        navController.addOnDestinationChangedListener(this)


        mViewModel = ViewModelProvider(this).get(SmartBinViewModel::class.java)

        // Create the observer which updates the UI.
        val smartBinsObserver = Observer<List<SmartBin>> { smartBins ->
            // Update the UI, in this case, a TextView.
            //nameTextView.text = newName
            //
            smartBins?.let {
                val clearBinIcon =   generateBitmapDescriptorFromRes(
                    this,
                    R.drawable.icon_garbage
                )
                val redBinIcon = generateBitmapDescriptorFromRes(this,R.drawable.icon_garbage_red)

                for (bin in smartBins){
                    if (TreeMap(bin.binFillLevels).lastEntry().value > 80.0){
                        val id = bin.id
                        val marker = smartBinMarkers[id]
                        marker?.setIcon(redBinIcon)
                    }else{
                        val id = bin.id
                        val marker = smartBinMarkers.get(id)
                        marker?.setIcon(clearBinIcon)
                    }
                }

            }
        }

        mViewModel.getSmartBins().observe(this,smartBinsObserver)


    }

    override fun onDestroy() {
        mViewModel.stopSmartBinMqttService()
        super.onDestroy()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.style_json
                )
            )
            if (!success) {
                Log.e(Utils.TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(Utils.TAG, "Can't find style. Error: ", e)
        }
        mMap = googleMap

        // centre map on UofG
        val uofG = LatLng(55.87219, -4.288222)
        val uniCentre = mMap.addMarker(MarkerOptions().position(uofG).title("UofG Main Building"))
        uniCentre.isVisible = false
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(uofG, 15.5F))

        // add markers for each smartBin
        setBinMarker()
    }

    private fun setBinMarker() {
        val binIconBitmap =   generateBitmapDescriptorFromRes(
            this,
            R.drawable.icon_garbage
        )
        for ((locationName,latLng) in Utils.binLocations){
            val location = LatLng(latLng.first,latLng.second)
            val markerOption = MarkerOptions().position(location).title(locationName).icon(binIconBitmap)
            val marker = mMap.addMarker(markerOption)
            marker.tag = locationName.toLowerCase().replace("\\s".toRegex(), "_")
            smartBinMarkers[marker!!.tag.toString()] = marker
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mapTypeNormal -> {
                mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            }
            R.id.mapTypeHybrid -> {
                mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
            }
            R.id.mapTypeSatellite -> {
                mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
            }
            R.id.mapTypeTerrain -> {
                mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
            }
            else -> return false
        }
        return false
    }

    private fun generateBitmapDescriptorFromRes(context: Context, resId: Int): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(context, resId)
        drawable!!.setBounds(
            0,
            0,
            drawable.intrinsicWidth,
            drawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        TODO("Not yet implemented")
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when(destination.id){
        }
    }

}
